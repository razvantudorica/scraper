import MySQLdb

class Database:
    def __init__(self, info):
        self.db = MySQLdb.connect(host=info['host'], user=info['user'], passwd=info['passwd'], db=info['db'], charset="utf8")
        self.cur = self.db.cursor()
        self.db.set_character_set('utf8')
        self.cur.execute('SET NAMES utf8;')
        self.cur.execute('SET CHARACTER SET utf8;')
        self.cur.execute('SET character_set_connection=utf8;')

    def getConnection(self):
        return self.db

    def getCursor(self):
        return self.cur
