# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class JobsItem(Item):
    title = Field()
    link = Field()
    site_id = Field()

    def save(self, db):
        cur = db.getCursor()
        con = db.getConnection()

        item = vars(self)['_values']

        try:
            cur.execute("INSERT INTO urls (site_id, url, status) VALUES (%s, %s, 0) ", (item['site_id'], item['link'],))
            con.commit()
        except Exception, e:
            print cur._last_executed
            print e

class EjobJob(Item):
    title = Field()
    description = Field()
    link = Field()
    company = Field()
    valability = Field()
    job_type = Field()
    salary = Field()
    salary_currency = Field()
    languages = Field()
    added = Field()
    address = Field()
    posted_at = Field()
    url_id = Field()
    updated = Field()
    category_id = Field()

    def save(self, db):
        item = vars(self)['_values']

        cur = db.getCursor()
        con = db.getConnection()

        try:
            cur.execute("INSERT INTO jobs (url_id, category_id, title, content, job_type, salary, salary_currency, "
                        "valability, address, posted_at, company) "
                            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ",
                            (item['url_id'], item['category_id'], item['title'], item['description'], item['job_type'],
                             item['salary'], item['salary_currency'], item['valability'], item['address'],
                             item['posted_at'], item['company']))

            cur.execute("UPDATE urls SET status=2, updated_at='{0}' "
                        "WHERE id={1}".format(item['updated'], item['url_id']))
            con.commit()
        except Exception, e:
            print cur._last_executed
            print e

            try:
                cur.execute("UPDATE urls SET status=3, updated_at=%s, error=%s WHERE id=%s", (item['updated'], e, item['url_id'], ) )
                con.commit()
            except Exception, ex1:
                print "Could not update the url status"
                print ex1


