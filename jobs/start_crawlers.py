from subprocess import Popen
import MySQLdb
import time
import sys
import jobs.settings as settings

info = settings.MYSQL
db = MySQLdb.connect(host=info['host'], user=info['user'], passwd=info['passwd'], db=info['db'])

cur = db.cursor()
cur.execute("SELECT id, url, domain, crawler_name FROM sites WHERE active=1")

for row in cur.fetchall() :
    site_id = row[0]
    site_url = row[1]
    domain = row[2]
    crawler_name = row[3]

    list_cmd = ["scrapy", "crawl", crawler_name, "-a", "url={0}".format(site_url), "-a", "domain={0}".format(domain), "-a", "site_id={0}".format(site_id)]
    Popen(list_cmd).wait()


time.sleep(5)

sql = "SELECT u.url, s.crawler_name FROM `urls` u " \
      "LEFT JOIN sites s ON u.site_id = s.id " \
      "WHERE u.status=0 ORDER BY u.id DESC"

cur.execute(sql)

for row in cur.fetchall():
    url = row[0]
    crawler_name = row[1] + '_job'
    list_cmd = ["scrapy", "crawl", crawler_name, "-a", "url={0}".format(url)]
    Popen(list_cmd).wait()
    time.sleep(2)
    # sys.exit()
