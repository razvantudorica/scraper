The JobScraper crawls different job websites and extract the jobs.
Currently the JobScraper crawls
- ejobs.ro, software section
- monster.co.uk, IT section

Installation:

sudo apt-get install -y python-dev libffi-dev libxml2-dev libxslt1-dev


Running:
# edit jobs/jobs/settings.py and change database credentials
cd jobs
python start_crawlers.py


For the future:
- more sources
- more categories
- language guessing

For language guessing:
sudo apt-get install -y mercurial

hg clone https://bitbucket.org/amentajo/lib3to2
hg clone https://bitbucket.org/spirit/guess_language

cd lib3to2
python setup.py build
python setup.py install

cd ../guess_language
python setup.py build
python setup.py install

==============

In sites table should be added the sites. The crawler_name should be the same as the one defined spiders folder.
The crawler for the job should be crawler_name from the sites table concatenated with '_job'. Eg: ejobs_job, monster_it_job...

