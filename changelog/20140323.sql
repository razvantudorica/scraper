ALTER TABLE  `sites` ADD  `category_id` INT NULL DEFAULT NULL AFTER  `id` ,ADD INDEX (  `category_id` ) ;

ALTER TABLE  `sites` ADD FOREIGN KEY (  `category_id` ) REFERENCES  `job_crawler`.`category` (`id`) ON DELETE SET NULL ON UPDATE SET NULL ;

update sites set category_id=2;
